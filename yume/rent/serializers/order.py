from django.db.models import Q
from rest_framework import serializers

from rent.models import OrderProduct, Order, Product
from rent.serializers import ProductSerializer


class OrderSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ('id', 'price', 'products', 'returned_at', 'created_at')

    def get_products(self, obj):
        products = Product.objects.filter(order_products__rent=obj)
        return ProductSerializer(products, many=True).data


class OrderCreateSerializer(serializers.ModelSerializer):
    products = serializers.ListField(
        child=serializers.IntegerField(), write_only=True, required=True
    )

    class Meta:
        model = Order
        fields = ['id', 'price', 'returned_at', 'products']

    def validate(self, data):
        product_ids = data.get('products', [])
        if not product_ids:
            raise serializers.ValidationError("Product list cannot be empty.")

        # Один товар не может быть включен в аренду более одного раза
        if len(set(product_ids)) != len(product_ids):
            raise serializers.ValidationError("Product list cannot contain duplicate products.")

        # Fetch all products once and check if any are currently rented
        products = Product.objects.filter(id__in=product_ids)
        if len(products) != len(product_ids):
            raise serializers.ValidationError("One or more products not found.")

        # Аренды с одинаковыми товарами не могут пересекаться во времени
        rented_products = products.filter(
            Q(order_products__rent__isnull=False) & Q(order_products__rent__returned_at__isnull=True)
        ).values_list('id', flat=True)

        if rented_products:
            raise serializers.ValidationError(f'Products with IDs {list(rented_products)} are already rented.')

        data['products'] = products
        return data

    def create(self, validated_data):
        products_data = validated_data.pop('products')
        order = Order.objects.create(**validated_data)
        for product in products_data:
            OrderProduct.objects.create(product=product, rent=order)
        return order


class OrderReturnSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'returned_at')

    def update(self, instance, validated_data):
        instance.returned_at = validated_data.get('returned_at')
        instance.save()
        return instance

