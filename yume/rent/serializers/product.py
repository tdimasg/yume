from django.db.models import Sum
from rest_framework import serializers

from rent.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'created_at', 'updated_at')


class ProductStatisticsSerializer(serializers.Serializer):
    total_price = serializers.SerializerMethodField()
    free_intervals = serializers.SerializerMethodField

    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'created_at', 'updated_at')

    def get_total_price(self, obj: Product):
        return obj.order_products.aggregate(total_price=Sum('product__price'))['total_price']

    def get_free_intervals(self, obj: Product):
        # start_time = obj.created_at
        return 'Not implemented yet'
