from django.db import models


class TimestampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Product(TimestampMixin):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Price of product")

    def __str__(self):
        return self.name

    @property
    def is_rented(self):
        return self.order_products.filter(rent__returned_at__isnull=True).exists()


class Order(TimestampMixin):
    price = models.DecimalField(max_digits=10, decimal_places=2)
    returned_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'Rent of{self.id}'


class OrderProduct(TimestampMixin):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='order_products')
    rent = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_products')

    class Meta:
        unique_together = ('product', 'rent')

    def __str__(self):
        return f'Rent for {self.product.name}'
