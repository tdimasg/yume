from django.utils import timezone
from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rent.models import Order
from rent.serializers.order import OrderSerializer, OrderCreateSerializer, OrderReturnSerializer
from yume.utils.decorator import response_code_wrapper


@method_decorator(response_code_wrapper(), name='dispatch')
class OrderViewSet(viewsets.GenericViewSet, viewsets.mixins.ListModelMixin, viewsets.mixins.CreateModelMixin,
                   viewsets.mixins.RetrieveModelMixin,
                   viewsets.mixins.DestroyModelMixin):

    def get_queryset(self):
        return Order.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            return OrderCreateSerializer
        if self.action == 'return_products':
            return OrderReturnSerializer
        return OrderSerializer


    @action(detail=True, methods=['put'])
    def return_products(self, request, *args, **kwargs):
        order = self.get_object()
        order.returned_at = timezone.now()
        order.save()
        return Response({'status': 'success'})
