from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework.decorators import action

from rent.models import Product
from rent.serializers import ProductSerializer
from rent.serializers.product import ProductStatisticsSerializer
from yume.utils.decorator import response_code_wrapper
from rest_framework.response import Response


@method_decorator(response_code_wrapper(), name='dispatch')
class ProductViewSet(viewsets.GenericViewSet, viewsets.mixins.ListModelMixin,
                     viewsets.mixins.CreateModelMixin,
                     viewsets.mixins.RetrieveModelMixin, viewsets.mixins.UpdateModelMixin,
                     viewsets.mixins.DestroyModelMixin):

    def get_queryset(self):
        return Product.objects.all()

    def get_serializer_class(self):
        if self.action == 'statistics':
            return ProductStatisticsSerializer
        return ProductSerializer

    @action(detail=False, methods=['get'])
    def statistics(self, request):
        #: предоставить сумму аренды для каждого продукта и таблицу с временными интервалами, когда продукт не занят
        products = self.get_queryset()
        serializer = self.get_serializer(products, many=True)
        return Response(serializer.data)
