from copy import deepcopy
from functools import wraps

from rest_framework.status import is_success

from yume.utils import codes


def response_code_wrapper(response_result_key='result'):
    """
    Decorator to make a view only accept request with required http method.
    :param required http method.
    """

    def decorator(func):
        @wraps(func)
        def inner(request, *args, **kwargs):
            response = func(request, *args, **kwargs)
            try:
                if is_success(response.status_code):
                    if hasattr(response, 'data'):
                        # PopUp response
                        if isinstance(response.data,
                                      dict) and response.data.get('code',
                                                                  0) in (
                                codes.POPUP_ERROR, codes.BAD_REQUEST,
                                codes.DIALOG_ERROR):
                            return response

                        # Response result and code status in one level
                        data = deepcopy(response.data)

                        response.data = {
                            response_result_key: data,
                            'code': codes.OK
                        }

            except Exception as exc:
                print(f'Exception: {str(exc)}')
            return response

        return inner

    return decorator
