Django==5.0.4
djangorestframework==3.15.1
django-filter==24.2
drf-spectacular==0.27.2
django-cors-headers==4.3.1
