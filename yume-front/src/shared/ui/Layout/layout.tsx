import {Box, Container, CssBaseline, Toolbar} from '@mui/material';
import DrawerComponent from "src/shared/ui/Layout/DrawerComponent.tsx";

interface LayoutProps {
    children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <DrawerComponent />
            <Box component="main" sx={{p: 3}}>
                <Toolbar/> {/* This adds top spacing for the content under the app bar */}
                <Container>{children}</Container>
            </Box>
        </>
    );
};

export default Layout;
