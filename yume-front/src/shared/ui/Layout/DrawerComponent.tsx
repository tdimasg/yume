import OrderIcon from "@mui/icons-material/Assignment";
import StatisticIcon from "@mui/icons-material/BarChart";
import ProductIcon from "@mui/icons-material/ShoppingCart";
import React from "react";
import { Link as RouterLink } from 'react-router-dom';
import {
    Drawer,
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    IconButton,
    Toolbar,
    AppBar,
    Typography,
    CssBaseline
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import HomeIcon from '@mui/icons-material/Home';

const DrawerComponent: React.FC = () => {
    const [isOpen, setIsOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setIsOpen(!isOpen);
    };

    const drawerWidth = 240;

    const drawerItems = [
        { name: 'Home', path: '/', icon: <HomeIcon /> },
        { name: 'Order', path: '/order', icon: <OrderIcon /> },
        // { name: 'Product', path: '/product', icon: <ProductIcon /> },
        { name: 'Statistic', path: '/statistic', icon: <StatisticIcon /> },
    ];

    return (
        <>
            <CssBaseline />
            <AppBar color="primary">
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                        My Application
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="temporary"
                open={isOpen}
                onClose={handleDrawerToggle}
                ModalProps={{
                    keepMounted: true,
                }}
                sx={{
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                }}
            >
                <List>
                    {drawerItems.map((item) => (
                        <ListItem button key={item.name} component={RouterLink} to={item.path} onClick={handleDrawerToggle}>
                            <ListItemIcon>{item.icon}</ListItemIcon>
                            <ListItemText primary={item.name} />
                        </ListItem>
                    ))}
                </List>
            </Drawer>
        </>
    );
};

export default DrawerComponent;
