export type ResponseData<T> = {
  code: number
  message?: string
  result: T
}

export type PaginatedResponseData<T> = {
  items: T[]
  total: number
  page: string
  size: string
  pages: number
}

export type PaginatedStateData<T> = {
  count: number
  results: T
}
