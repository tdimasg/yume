import axios, {AxiosResponse} from 'axios'
import {BaseURL} from './baseURL.ts'
import {ResponseData} from './responseData.ts'

export const request = axios.create({
    baseURL: BaseURL,
    headers: {
        'Content-Type': 'application/json',
    },
})



export const handleError = <T>(data: ResponseData<T>): void => {
    if (data.code !== 0) {
        throw new Error(data.message || 'There is some error. Try again')
    }
}

export const get = <T>(url: string): Promise<AxiosResponse<T>> =>
    request.get(url)

export const post = <Req, Res>(
    url: string,
    data: Req
): Promise<AxiosResponse<Res>> => request.post(url, data)
