import React from "react";
import Button from "@mui/material/Button";
import OrderTable from "src/entities/orders/ui/table.tsx";
import CreateModal from "src/features/orders/ui/createModal";

const Order: React.FC = () => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false);
    }

    return <div>
        <Button onClick={handleOpen}>Create Order</Button>
        <OrderTable/>
        <CreateModal open={open} onClose={handleClose}/>
    </div>;
}

export default Order;
