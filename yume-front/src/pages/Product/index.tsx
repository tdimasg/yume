import React from "react";
import Button from "@mui/material/Button";
import ProductTable from "src/entities/products/ui/table.tsx";
import CreateModal from "src/features/products/ui/createModal";

const Product: React.FC = () => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (<>
        <Button onClick={handleOpen}>Create Product</Button>

        <ProductTable/>
        <CreateModal open={open} onClose={handleClose}/>
    </>);
}

export default Product;
