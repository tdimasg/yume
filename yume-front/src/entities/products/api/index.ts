import {AxiosResponse} from 'axios'
import {Product} from "src/entities/products/model";
import {request} from 'src/shared/api/axios'
import {ResponseData} from 'src/shared/api/responseData.ts'

export const getProducts = (): Promise<AxiosResponse<ResponseData<Product[]>>> => {
    const url = `/products/`
    return request.get(url)
}
