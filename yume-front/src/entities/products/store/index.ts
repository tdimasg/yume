import {useQuery} from '@tanstack/react-query'
import {AxiosResponse} from 'axios'
import {Order} from "src/entities/orders/model";
import {getProducts} from "src/entities/products/api";
import {Product} from "src/entities/products/model";
import {ResponseData} from 'src/shared/api/responseData.ts'

export const useProducts = () => {
    const {data,} = useQuery<
        unknown,
        Error,
        Product[]
    >({
        queryKey: ['products'],
        queryFn: () =>
            getProducts().then((res: AxiosResponse<ResponseData<Product[]>>) => {
                return res.data.result
            }),
    })

    return {
        data: data || [],
    }
}
