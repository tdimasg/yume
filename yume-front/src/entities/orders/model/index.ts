type Products = {
    id: string
    name: string
    price: string
    created_at: string
    updated_at: string
}

export type Order = {
    id: string
    price: string
    products: Products[]
    returned_at: string
    created_at: string
}



