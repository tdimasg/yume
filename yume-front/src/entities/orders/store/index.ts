import {useQuery} from '@tanstack/react-query'
import {AxiosResponse} from 'axios'
import {getOrders} from "src/entities/orders/api";
import {Order} from "src/entities/orders/model";
import {ResponseData} from 'src/shared/api/responseData.ts'

export const useOrders = () => {
    const {data, } = useQuery<
        unknown,
        Error,
        Order[]
    >({
        queryKey: ['orders'],
        queryFn: () =>
            getOrders().then((res: AxiosResponse<ResponseData<Order[]>>) => {
                return res.data.result
            }),
    })

    return {
        data: data || [],
    }
}

// export const useDeleteCategory = () => {
//   const mutation = useMutation({
//     mutationFn: (id: string | null) => {
//       if (id == null) {
//         message.error('doesnt settled id')
//       }
//
//       return deleteCategory(id as string).then(
//         (res: AxiosResponse<unknown>) => {
//           return res.data
//         }
//       )
//     },
//   })
//
//   return mutation
// }
//
// export const useCreateCategory = () => {
//   const mutation = useMutation({
//     mutationFn: (data: CreateCategoryRequest) => {
//       return createCategory(data).then((res: AxiosResponse<unknown>) => {
//         return res.data
//       })
//     },
//   })
//
//   return mutation
// }
