import {AxiosResponse} from 'axios'
import {Order} from "src/entities/orders/model";
import {request} from 'src/shared/api/axios'
import {ResponseData} from 'src/shared/api/responseData.ts'

export const getOrders = (): Promise<AxiosResponse<ResponseData<Order[]>>> => {
    const url = `/orders/`
    return request.get(url)
}
