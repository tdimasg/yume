import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import {useOrders} from "src/entities/orders/store";
import {format, parseISO} from 'date-fns'
import {ReturnOrder} from "src/features/orders/ui/returnOrder";


export default function OrderTable() {
    const {data} = useOrders()
    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 650}} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell align="right">Price</TableCell>
                        <TableCell align="right">Products</TableCell>
                        <TableCell align="right">returned at</TableCell>
                        <TableCell align="right">Return</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((order) => (
                        <TableRow
                            key={order.id}
                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                        >
                            <TableCell component="th" scope="row">
                                {order.id}
                            </TableCell>
                            <TableCell align="right">{order.price}</TableCell>
                            <TableCell align="right">{order.products.map(product =>{
                                return product.name
                            })}</TableCell>
                            <TableCell align="right">
                                {!!order?.returned_at ?format(parseISO(order.returned_at), 'yyyy-MM-dd HH:mm:ss') : 'not returned'}
                            </TableCell>
                            <TableCell align="right">
                                <ReturnOrder id={Number(order.id)} return_at={order.returned_at}/>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}