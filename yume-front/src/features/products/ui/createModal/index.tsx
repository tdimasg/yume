import * as React from 'react';
import { Box, Button, Typography, Modal, IconButton, Fade } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: { xs: '80%', sm: 500 }, // Increased and responsive width
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

type Props = {
    open: boolean;
    onClose: () => void;
    children?: React.ReactNode;
};

export default function CreateModal({ open, onClose, children }: Props) {
    return (
        <div>
            <Modal
                open={open}
                onClose={onClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                closeAfterTransition
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <IconButton
                            aria-label="close"
                            onClick={onClose}
                            sx={{ position: 'absolute', right: 8, top: 8 }}
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography id="modal-modal-title" variant="h6" component="h2" sx={{ mb: 2 }}>
                            Create Product
                        </Typography>
                        {children || (
                            <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                                Details about creating an order...
                            </Typography>
                        )}
                        <Button variant="contained" onClick={onClose} sx={{ mt: 2 }}>
                            Close
                        </Button>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}
