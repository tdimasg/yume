export type ReturnOrder = {
    id: string;
    return_at: string;
}

export type CreateOrder = {
    price: string;
    products: number[];
    onClose: () => void;
}