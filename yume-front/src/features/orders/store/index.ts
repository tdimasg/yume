import {useMutation} from '@tanstack/react-query'
import {AxiosResponse} from 'axios'
import {queryClient} from "src/App.tsx";
import {createOrder, returnOrder} from "src/features/orders/api";
import {CreateOrder, ReturnOrder} from "src/features/orders/model";

export const useReturnOrder = () => {
    const mutation = useMutation({
        mutationFn: (data: ReturnOrder) => {
            return returnOrder(data).then((res: AxiosResponse<unknown>) => {
                return res.data
            })
        },
        onSuccess: () => {
            queryClient.invalidateQueries({queryKey: ['orders']})
        }
    })

    return mutation
}

export const useCreateOrder = () => {
    const mutation = useMutation({
        mutationFn: (data: CreateOrder) => {
            return createOrder(data).then((res: AxiosResponse<unknown>) => {
                return res.data
            })
        },
        onSuccess: (data) => {
            queryClient.invalidateQueries({queryKey: ['orders']})
            data.onClose()
        }
    })

    return mutation
}