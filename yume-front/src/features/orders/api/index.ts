import {AxiosResponse} from "axios";
import {CreateOrder, ReturnOrder} from "src/features/orders/model";
import {request} from "src/shared/api/axios.ts";

export const returnOrder = (
    data: ReturnOrder
): Promise<AxiosResponse<unknown>> => {
    const url = `/orders/${data.id}/return_products/`
    return request.put(url, {
        returned_at: data.return_at
    })
}

export const createOrder = (
    data: CreateOrder
): Promise<AxiosResponse<unknown>> => {
    return request.post('/orders/', data)
}