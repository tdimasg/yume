import CloseIcon from '@mui/icons-material/Close';
import {
    Box,
    Button,
    Fade,
    FormControl,
    IconButton,
    InputLabel,
    MenuItem,
    Modal,
    Select,
    TextField,
    Typography
} from '@mui/material';
import * as React from "react";
import {useEffect} from "react";
import {Controller, useForm} from 'react-hook-form';
import {useProducts} from "src/entities/products/store";
import {useCreateOrder} from "src/features/orders/store";

interface FormValues {
    price: string;
    products: number[];
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: {xs: '80%', sm: 500}, // Responsive width
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

type Props = {
    open: boolean;
    onClose: () => void;
    children?: React.ReactNode;
};

export default function CreateModal({open, onClose}: Props) {
    const {mutate, error} = useCreateOrder()
    const {control, handleSubmit, formState: {errors}} = useForm<FormValues>({
        defaultValues: {
            price: '',
            products: []
        }
    });

    const {data, isPending} = useProducts()
    
    const onSubmit = (data: FormValues) => {
        mutate({...data, onClose})
    };

    return (
        <div>
            <Modal
                open={open}
                onClose={onClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                closeAfterTransition
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <IconButton
                            aria-label="close"
                            onClick={onClose}
                            sx={{position: 'absolute', right: 8, top: 8}}
                        >
                            <CloseIcon/>
                        </IconButton>
                        <Typography id="modal-modal-title" variant="h6" component="h2" sx={{mb: 2}}>
                            Create Order
                        </Typography>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Controller
                                name="price"
                                control={control}
                                render={({field}) => (
                                    <TextField
                                        onChange={field.onChange}
                                        value={field.value}
                                        label="Price"
                                        type="text"
                                        error={!!errors.price}
                                        helperText={errors.price ? errors.price.message : null}
                                    />
                                )}
                            />
                            <FormControl fullWidth>
                                <InputLabel id="products-label">Products</InputLabel>
                                <Controller
                                    name="products"
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            onChange={field.onChange}
                                            value={field.value}
                                            labelId="products-label"
                                            label="Products"
                                            multiple
                                            renderValue={(selected) => selected.join(', ')}
                                            error={!!errors.products}
                                        >
                                            {data.map((option) => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    Product {option.name}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    )}
                                />
                            </FormControl>
                            {error && <Typography color="error">{error?.response?.data?.non_field_errors?.[0]}</Typography>}
                            <Button type="submit" variant="contained" color="primary">
                                Submit
                            </Button>
                        </form>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}
