import Button from "@mui/material/Button";
import {useReturnOrder} from "src/features/orders/store";

type Props = {
    id: number
    return_at: string
}

export const ReturnOrder = ({id, return_at}: Props) => {
    const {mutate, isPending} = useReturnOrder()

    const returnOrder = () => {
        mutate({
            id: id.toString(),
            return_at: new Date().toISOString()
        })
    }

    if (return_at) {
        return <Button disabled>Returned</Button>
    }

    return <Button disabled={isPending} onClick={returnOrder}>{isPending ? 'Loading': 'Return Order'}</Button>
}