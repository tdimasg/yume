import {ThemeProvider} from '@mui/material/styles';

import {MutationCache, QueryClient, QueryClientProvider,} from '@tanstack/react-query'
import {Route, Routes} from 'react-router-dom';
import Layout from "src/shared/ui/Layout/layout.tsx";
import Home from './pages/Home';
import Order from './pages/Order';
import Product from './pages/Product';
import Statistic from './pages/Statistic';
import theme from './theme/theme.ts';

export const queryClient = new QueryClient({
    mutationCache: new MutationCache({}),
})

const App: React.FC = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={theme}>
                <Layout>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/order" element={<Order/>}/>
                        <Route path="/product" element={<Product/>}/>
                        <Route path="/statistic" element={<Statistic/>}/>
                    </Routes>
                </Layout>
            </ThemeProvider>
        </QueryClientProvider>
    );
}

export default App;
